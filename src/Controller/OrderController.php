<?php

namespace Drupal\tapin\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Class OrderController.
 */
class OrderController extends ControllerBase {

  /**
   * Getorder.
   *
   * @return array
   *   Return Hello string.
   */
  public function getOrder() {
    return [
      '#markup' => '<div id="topin-main__react"></div>',
      '#attached' => [
        'library' => 'tapin/tapin'
      ],
      '#cache' => [
        'max-age' => 0,
      ],
    ];
  }
}
