<?php

namespace Drupal\tapin\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Returns responses for tapin routes.
 */
class TapinController extends ControllerBase {

  /**
   * Builds the response.
   */
  public function build() {
    return [
      '#markup' => '<div id="topin-main__react"></div>',
      '#attached' => [
        'library' => 'tapin/tapin'
      ],
      '#cache' => [
        'max-age' => 0,
      ],
    ];
  }
}
