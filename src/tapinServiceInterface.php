<?php

namespace Drupal\tapin;

/**
 * Interface tapinServiceInterface.
 */
interface TapinServiceInterface {

  /**
   * @param $page
   * @param $limit
   * @param $order_id
   *
   * @return array
   */
  public function getOrder($page, $limit, $order_id);

  /**
   * @param $data
   *
   * @return array
   */
  public function updateOrder($data);

}
