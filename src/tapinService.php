<?php

namespace Drupal\tapin;

use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Class tapinService.
 */
class tapinService implements tapinServiceInterface {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new tapinService object.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * @return array
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getOrder($page = 0, $limit = 10, $order_id = NULL) {
    $total = $this->entityTypeManager->getStorage('commerce_order')->getQuery();
    $total->condition('state', 'fulfillment');
    $total->condition('field_tapin_check', 0, '=');
    if ($order_id) {
      $total->condition('order_number', $order_id, '=');
    }
    $total->count();
    $totals = $total->execute();


    $query = $this->entityTypeManager->getStorage('commerce_order')->getQuery();
    $query->condition('state', 'fulfillment');
    $query->condition('field_tapin_check', 0, '=');
    if ($order_id) {
      $query->condition('order_number', $order_id, '=');
    }
    $query->sort('changed', 'DESC');
    $query->range($page, $limit);

    $order_ids = $query->execute();
    $orders = $this->entityTypeManager->getStorage('commerce_order')
      ->loadMultiple($order_ids);

    $Order = [];
    foreach ($orders as $value) {
      $customerId = $value->get('billing_profile')->target_id;
      $customer = $this->entityTypeManager->getStorage('profile')
        ->load($customerId);
      //      dump($customer);die();
      $profile = [
        'country_code' => $customer->get('address')->country_code,
        'first_name' => $customer->get('address')->given_name,
        'last_name' => $customer->get('address')->family_name,
        'mobile_number' => $customer->get('field_text_mobile')->value,
        'address' => $customer->get('address')->address_line1,
        'province' => $customer->get('address')->administrative_area,
        'city' => $customer->get('address')->locality,
        'postal_code' => $customer->get('address')->postal_code,
      ];
      $orderItem = $this->entityTypeManager->getStorage('commerce_order_item')
        ->load($value->get('order_items')->getValue()[0]['target_id']);
      $prodoct = [
        'product_id' => $orderItem->get('purchased_entity')
          ->getValue()[0]['target_id'],
        'quantity' => $orderItem->get('quantity')->getValue()[0]['value'],
        'title' => $orderItem->get('title')->getValue()[0]['value'],
      ];
      $Order[] = [
        'order_id' => $value->get('order_number')->value,
        'order_owner' => $value->get('uid')->target_id,
        'order_number' => $value->getOrderNumber(),
        'order_total' => $value->getTotalPrice()->getNumber() / 1,
        'order_date' => $value->getPlacedTime(),
        'profile' => $profile,
        'product' => $prodoct,
      ];
    }
    return [
      'total' => $totals,
      'page' => $page,
      'total_page' => ceil($totals / $limit),
      'data' => $Order,
    ];
  }

  /**
   * @param $id
   *
   * @return void
   */
  public function shipmentsProfile($id) {

  }

  /**
   * @param $uid
   *
   * @return mixed
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getUserName($uid) {
    //get user name
    $user = $this->entityTypeManager->getStorage('user')
      ->load($uid);
    return $user->get('name')->value;
  }

  /**
   * @param $data
   *
   * @return bool
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function updateOrder($data) {
    // update order set field_tapin_check value
    $order = $this->entityTypeManager->getStorage('commerce_order')
      ->load($data['order_id']);
    $order->set('field_barcode_tapin', $data['barcode']);
    $order->set('field_tapin_check', 1);
    $order->set('field_tapin_order_id', $data['field_tapin_order_id']);
    $order->save();
    return TRUE;
  }

}
